const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// Controllers

// This controller will create or register a user on our database.
module.exports.userRegistration = (request, response) => {

	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result !== null){
			return response.send("The email is already taken!");
		}else{
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			// save to database
			newUser.save()
			.then(save => {
				return response.send("You are now registered in our website!")
			})
			.catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error);
	})
}


// User Authentication
module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	// Possible scenarios in logging in
		// 1. Email is not yet registered
		// 2. Email is registered byt the password is wrong

	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return response.send("Email is not yet registered. Register first before logging in!");
		}else{
			// We have to verify if the password is correct
			// the CompareSync() method is used to compare a non encrypted password to the encrypted password
			// it returns a boolean value. If match, true.
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			}else{
				return response.send("Password is incorrect!");
			}
		}
	})
	.catch(error => {
		return response.send(error);
	})
}



// Get Profile
module.exports.getProfile = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);	

	console.log(userData);

	return User.findById(userData._id)
	.then(result => {
		//avoid to expose sensitive information such as password
		result.password = "Confidential";

		return response.send(result);
	})
}



// Controller for User Enrollment
	// 1. We can get the Id of the user by getting decoding the jwt
	// 2. We can get the course ID by using the request params

module.exports.enrollCourse = (request, response) => {
	// 1st, we have to get the user ID and the course ID

	// 2nd, decode the token to extract/unpack the payload
	const userData = auth.decode(request.headers.authorization);

	// Get the courseId by targetting the params in the url
	const courseId = request.params.courseId;


	if(userData.isAdmin === true){
		return response.send("Admins cannot enroll in class.");
	}
	else{

		

		Course.findOne({_id: courseId})
		.then(result => {
			console.log(result);
			if(result === null){
				return response.send("Invalid Course ID. Course is not existing.")
			}
			else{
				// return response.send("Course is available.")

				// 2 things we need to do in this controller:
					// 1. Push the courseId in the enrollments property of the user.
					// 2 Push the user Id in the enrollees property of the course.		

				let isUserUpdated = User.findById(userData._id)
				.then(result => {
					if(result === null){
						return false
					}
					else{
						result.enrollments.push({courseId: courseId})

						return result.save()
						.then(save => true)
						.catch(error => false)
					}
				})

				let isCourseUpdated = Course.findById(courseId)
				.then(result => {
					if(result === null){
						return false
					}
					else{
						result.enrollees.push({userId: userData._id})

						return result.save()
						.then(save => true)
						.catch(error => false);
					}
				})

				if(isCourseUpdated && isUserUpdated){
					return response.send("The course is now enrolled.")
				}
				else{
					return response.send("There was an error during the enrollment. Please try again.")
				}
			}
		})
		.catch(error => {
			return response.send(error);
		})


		
	}
}