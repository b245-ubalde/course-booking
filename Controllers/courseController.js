const mongoose = require("mongoose");
const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) => {
	const adminData = auth.decode(request.headers.authorization);
	console.log(adminData);
	if(adminData.isAdmin == true){
		let input = request.body;

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});

		// saves the created object to our database
		return newCourse.save()
		// course successfully added
		.then(course => {
			console.log(course);
			response.send(course);
		})
		// course creation failed
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	}	
	else{
		return response.send(401, "Sorry. You're not authorized for this action.")
	}
}



// Create a controller wherein it will retrieve all the courses

module.exports.allCourses = (request,response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(!userData.isAdmin){
		return response.send("You don't have access to this route!");
	}
	else{
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}




// Create a controller wherein it will retrieve courses that are active.
module.exports.allActiveCourses = (request, response) =>{
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


/*
	Mini Activity

	Create a route wherein it can retrieve all inactive courses

	Make sue that the admin users only are the ones that can access this route.
*/


// Controller for retrieving all inactive courses
module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(userData.isAdmin !== true){
		return response.send("You dont have access for this route.");
	}
	else{
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}


// This controller will get the details of specific course
module.exports.courseDetails = (request, response) => {
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}




// This controller is for updating specific course
/*
	Business Logic:
		1. We are going to edit/update the course, that is stored in the params.
*/

module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body;

	if(userData.isAdmin !== true){
		return response.send("You dont have access for this route.");
	}
	else{
		Course.findOne({_id: courseId})
		.then(result => {
			if(result === null){
				return response.send("Course ID is invalid. Please try again.");
			}
			else{
				let updatedCourse = {
					name: input.name,
					description: input.description,
					price: input.price
				}

				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					return response.send(result)
				})
				.catch(error => response.send(error));
			}
		})
		.catch(error => response.send(error));
	}
}



// Controller for Archiving Courses
module.exports.archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body;


	if(userData.isAdmin !== true ){
		return response.send("Sorry. You do not have access for this page.");
	}
	else{
		Course.findOne({_id: courseId})
		.then(result => {
			if(result === null){
				return response.send("Invalid Course ID. Please input a valid Course ID.");
			}
			else{
				let archivedCourse = {
					isActive: input.isActive
				}

					console.log(archivedCourse);	
					Course.findByIdAndUpdate(courseId, archivedCourse, {new: true})
					.then(result => {
						return response.send(result)
					})
					.catch(error => {
						return response.send(error);
					})
				}
			})
			.catch(error => {
				return response.send(error);
			})


	}
}