const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js");


// Routes

// This route is responsible for the registration of the user.
router.post("/register", userController.userRegistration);

// This route is for the user authentication
router.post("/login", userController.userAuthentication);

// This route is for retrieving user's details by user's Id
router.get("/details", auth.verify, userController.getProfile);

// route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);

module.exports = router;