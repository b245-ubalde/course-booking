const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController.js");
const auth = require("../auth.js");


// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

// Router for all courses
router.get("/all", auth.verify, courseController.allCourses);


// Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses);

// Route for retrieving all inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses);




// [Routes with Params]
	// Divide route with params and routes without params

// Retrieving details of a specific course
router.get("/:courseId", courseController.courseDetails);

// Route for updating a certain product
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// Router for archiving courses
router.put("/archive/:courseId", auth.verify, courseController.archiveCourse);

module.exports = router;